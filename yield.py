#!/usr/bin/env python3

# difference between yield and return

def generator():
    for i in range(10):
        return i

print(generator()) # 0

def generator():
    for i in range(10):
        yield i

g = generator()
for i in g:
    print(i) # 0, 1, 2, 3, 4, 5, 6, 7, 8, 9