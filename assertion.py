#!/usr/bin/env python3

a = 10
assert a > 70, "The value of a is lesser than 70"

#equivalent to

if not a>70:
    raise AssertionError("The value of a is lesser than 70")