#!/usr/bin/env python3

with open("with.txt", "w") as f:
    f.write("Hello, world!")

with open("fibonacci.py", "w") as first_file:
    first_file.write("#!/usr/bin/env python3\n\n")
    first_file.write("def fib(n):\n")
    first_file.write("    a, b = 0, 1\n")
    first_file.write("    while a < n:\n")
    first_file.write("        yield a\n")
    first_file.write("        a, b = b, a + b\n")
    first_file.write("\n")
    first_file.write("for x in fib(1000):\n")
    first_file.write("    print(x, end=' ')\n")

with open("yield.py", "w") as y_file:
    y_file.write("#!/usr/bin/env python3\n\n")