#!/usr/bin/env python3

# break
for i in range(1,11):
    if i == 5:
        break
    print(i) # will skip everything after 4

# continue
for i in range(1,11):
    if i == 5:
        continue
    print(i) # will skip 5

    
