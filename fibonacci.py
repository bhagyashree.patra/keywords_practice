#!/usr/bin/env python3

def fib(n):
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a + b

for x in fib(1000):
    print(x, end=' ')
